import React from 'react';
import { View } from 'react-native';

import Tabs from 'react-native-tabs';

const headerpart = () => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <Tabs selected={this.state.page} style={{ backgroundColor: 'white' }}
                selectedStyle={{ color: 'red' }} onSelect={(el) => this.setState({ page: el.props.name })}>
                <Text name="first">First</Text>
                <Text name="second" selectedIconStyle={{ borderTopWidth: 2, borderTopColor: 'red' }}>Second</Text>
                <Text name="third">Third</Text>
            </Tabs>
        </View>
    )
};

export default headerpart;