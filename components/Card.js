import React from 'react';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import { StyleSheet, Dimensions, Image, TouchableWithoutFeedback, TouchableOpacity, View, Text, Button } from 'react-native';
import { Block, theme } from 'galio-framework';
import Icon from '../../walkingpledge/components/Icon';
import { argonTheme } from '../constants';
//import Icon1 from 'react-native-vector-icons/FontAwesome5';
import { Ionicons } from '@expo/vector-icons';

class Card extends React.Component {

  render() {

    console.log('[Render] card:-', this.props.item.freebees);
   
  //  const owneremail= "anshuman78@gmail1.com"
    const { navigation, item, horizontal, full, style, ctaColor, imageStyle, type, daysleft, Objective, pplCount, duration, description, loggedemail, owneremail} = this.props;
    console.log('Owner email is:-',this.props.owneremail)
    const imageStyles = [
      full ? styles.fullImage : styles.horizontalImage,
      imageStyle
    ];
    const cardContainer = [styles.card, styles.shadow, style];
    const imgContainer = [styles.imageContainer,
    horizontal ? styles.horizontalStyles : styles.verticalStyles,
    styles.shadow
    ];
    let giftProps = null;
    if (this.props.item.freebees) {
      giftProps = <Icon style={{ position: 'absolute' }} name='card-giftcard' family='FontAwesome5' size={20} />
    }

    let bgcolor;
    if(loggedemail===owneremail){
      console.log('Strings are equal');
      bgcolor = '#FFA07A'
    }else{
      console.log('Strings are unequal');
      bgcolor=''
    }
          

    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 20, backgroundColor: bgcolor}}>
      <View style={{ justifyContent: 'flex-start' }}>
        {giftProps}
        <Icon name='directions-walk' family='FontAwesome5' size={50} />
        <View style={{ alignSelf: 'flex-end' }}>
        <Text style={{position:'absolute', fontWeight:'bold', color:'#8a2be2'}}>{item.gametype}</Text>
          <Icon  color='#a9a9a9' name="clock" family='Foundation'  size={25}></Icon>
        </View>
      </View>

      <View style={{ paddingLeft: 40, paddingRight: 40, justifyContent: 'space-between', alignItems: 'center' }}>
        <TouchableOpacity onPress={() => navigation.navigate({
          routeName: 'GameDescription', params: {
            title: item.name,
            owner: item.owner,
            freebees: this.props.item.freebees,
            gameduration: item.gametype,
            pplCount: pplCount,
            description: item.description,
            startdate: item.startdate
          }
        })}>
        <View style={{paddingBottom:10}}>
          <Text>{item.name}</Text>
          <Text style={{ fontSize: 12 }}>Captain:{item.owner}</Text>
          </View>
          <Text style={{fontSize:12 , color: '#8a2be2'}}>Starts  {daysleft}</Text>
        </TouchableOpacity>
      </View>
      <View style={{ alignItems: 'center', justifyContent: 'space-between' }}>
        <Icon name="people" family='FontAwesome5' size={30} />
        <Text size={14} >{this.props.pplCount}</Text>
      </View>
    </View>
    );
  }
}

Card.propTypes = {
  item: PropTypes.object,
  horizontal: PropTypes.bool,
  full: PropTypes.bool,
  ctaColor: PropTypes.string,
  imageStyle: PropTypes.any,
}

const styles = StyleSheet.create({
  card: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 0,
    minHeight: 60,
    marginBottom: 16
  },
  cardTitle: {
    flex: 1,
    flexWrap: 'wrap',
    paddingBottom: 2
  },
  cardDescription: {
    paddingLeft: 20
  },
  imageContainer: {
    borderRadius: 3,
    elevation: 1,
    overflow: 'hidden',
  },
  image: {
    // borderRadius: 3,
  },
  horizontalImage: {
    height: 50,
    width: 'auto',
  },
  horizontalStyles: {
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
  },
  verticalStyles: {
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0
  },
  fullImage: {
    height: 215
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 0,
    shadowOpacity: 0.1,
    elevation: 0,
  },
});

export default withNavigation(Card);