import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment';
import Icon from '../components/Icon';
import PropTypes from 'prop-types';

class DateTime extends Component {

    constructor() {
        super();
        this.state = {
            isVisible: false,
            chosenDate:  Date
        }
    }

    handlePicker = (datetime) => {
        this.setState({
            isVisible: false,
            chosenDate: moment(datetime).format('MMMM, Do YYYY HH:mm')
        })
    }


    hidePicker = () => {
        this.setState({
            isVisible: false,

        })
    }

    showPicker = () => {
        this.setState({
            isVisible: true
        })
    }

    render() {

        return (
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity onPress={this.showPicker} style={{paddingRight:10}}>
                    <Icon
                        family="Entypo"
                        size={16}
                        name="calendar"
                    />
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.isVisible}
                    onConfirm={this.handlePicker}
                    onCancel={this.hidePicker}
                    minimumDate={new Date(Date.now() + (24 * 60 * 60 * 1000))}
                />
                <Text>
                    {this.state.chosenDate}
                </Text>
            </View>
        )
    }

};

DateTime.defaultProps = {
    chosenDate: new Date(Date.now() + (24 * 60 * 60 * 1000))
  }

DateTime.propTypes = {
    chosenDate: PropTypes.instanceOf(Date)
  };


export default DateTime;