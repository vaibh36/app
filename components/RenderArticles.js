import React,{useCallback} from 'react';
import Card from '../components/Card';
import {FlatList} from 'react-native';
import {getUpcomingData} from '../store/actions/UpcominggamescreenActions';
import { useDispatch } from 'react-redux';
const renderarticles = (props)=>{

    const {startdate,daysleft,gift,pplCount,duration,item, componentName,upComingData} = props;
    const [refreshing, setRefreshing] = React.useState(false);
    const dispatch = useDispatch()
    const newFunc = useCallback(() => {
        console.log('Inside useCallback of Render Articles')
        if(componentName==='getUpcomingGamesData'){
            dispatch(getUpcomingData());
        }
        setRefreshing(false);
      },[])

    handleRefresh = () => {
        setRefreshing(true);
        newFunc();
      }

    return(
        <FlatList refreshing={refreshing} onRefresh={this.handleRefresh} keyExtractor={item => item.id} data={upComingData} renderItem={({ item }) => (
            <Card startdate={item.startdate} daysleft={item.message} gift={item.freebees} pplCount={item.pplCount} duration={item.gameduration} item={item}  ></Card>
          )}
          />
    )

};

export default renderarticles;