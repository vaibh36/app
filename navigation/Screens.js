import React from "react";
import { Easing, Animated } from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
  createMaterialTopTabNavigator
} from "react-navigation";
import { Block } from "galio-framework";
// screens
import Home from "../screens/Home";
import Onboarding from "../screens/Onboarding";
import Pro from "../screens/Pro";
import Profile from "../screens/Profile";
import Register from "../screens/Register";
import Elements from "../screens/Elements";
import Articles from "../screens/Articles";
import GameDescription from '../screens/GameDescription';
import NewGame from '../screens/NewGame';
import GameCreated from '../screens/GameCreated'
// drawer
import Menu from "./Menu";
import DrawerItem from "../components/DrawerItem";
import CompletedGames from '../screens/CompletedGames';
import CurrentGames from '../screens/CurrentGames';
import UpcomingGames from '../screens/UpcomingGames';
import Login from '../screens/Login';
// header for screens
import Header from "../components/Header";
import GameJoin from '../screens/GameJoiningSreen';

const transitionConfig = (transitionProps, prevTransitionProps) => ({
  transitionSpec: {
    duration: 400,
    easing: Easing.out(Easing.poly(4)),
    timing: Animated.timing
  },
  screenInterpolator: sceneProps => {
    const { layout, position, scene } = sceneProps;
    const thisSceneIndex = scene.index;
    const width = layout.initWidth;

    const scale = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
      outputRange: [4, 1, 1]
    });
    const opacity = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
      outputRange: [0, 1, 1]
    });
    const translateX = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex],
      outputRange: [width, 0]
    });

    const scaleWithOpacity = { opacity };
    const screenName = "Search";

    if (
      screenName === transitionProps.scene.route.routeName ||
      (prevTransitionProps &&
        screenName === prevTransitionProps.scene.route.routeName)
    ) {
      return scaleWithOpacity;
    }
    return { transform: [{ translateX }] };
  }
});

const ElementsStack = createStackNavigator({
  Elements: {
    screen: Elements,
    navigationOptions: ({ navigation }) => ({
      header: <Header title="Elements" navigation={navigation} />
    })
  }
},{
  cardStyle: {
    backgroundColor: "#F8F9FE"
  },
  transitionConfig
});

const ArticlesStack = createStackNavigator({
  Articles: {
    screen: Articles,
    navigationOptions: ({ navigation }) => {
      let header = <Header title="Health Instalment" navigation={navigation} />
     return{ header}
    }
  }
},{
  cardStyle: {
    backgroundColor: "#F8F9FE"
  },
  transitionConfig
});

const ProfileStack = createStackNavigator(
  {
    Profile: {
      screen: Profile,
      navigationOptions: ({ navigation }) => ({
        header: (
          <Header white transparent title="Profile" iconColor={'#FFF'} navigation={navigation} />
        ),
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);


const HomeStack = createStackNavigator(
  {

    // Login: {
    //   screen: Login,
    //   navigationOptions: ({ navigation }) => {
    //    let header = <Header  title="Login" navigation={navigation} />
    //     return {header}
    //   }
     
    // },


    Home: {
      screen: Home,
      navigationOptions: ({ navigation }) => {
       let header = <Header search options title="Home" navigation={navigation} />
        return {header}
      }
     
    },
    NewGame: {
      screen: NewGame,
      navigationOptions: ({ navigation }) => {
       let header = <Header  title="NewGame" navigation={navigation} />
        return {header}
      }
     
    },
    GameCreated: {
      screen: GameCreated,
      navigationOptions: ({ navigation }) => {
       let header = <Header  title="GameCreated" navigation={navigation} />
        return {header}
      }
    },
    GameDescription: {
      screen: GameDescription,
      navigationOptions: ({ navigation }) => ({
        header: (
          <Header  title="Game Description" navigation={navigation} />
        ),
        headerTransparent: true,
        
      })
    },
    GameJoin: {
      screen: GameJoin,
      navigationOptions: ({ navigation }) => {
       let header = <Header  title="GameJoin" navigation={navigation} />
        return {header}
      }
    },
    CompletedGames:{
      screen: CompletedGames,
      navigationOptions: ({ navigation }) => ({
        header: (
          <Header search options  title="Completed Games" navigation={navigation} />
        ),
        headerTransparent: true,
        
      })
    },
    CurrentGames:{
      screen: CurrentGames,
      navigationOptions: ({ navigation }) => ({
        header: (
          <Header search options  title="Current Games" navigation={navigation} />
        ),
        headerTransparent: false,
        
      })
    },
    UpcomingGames:{
      screen: UpcomingGames,
      navigationOptions: ({ navigation }) => ({
        header: (
          <Header search options  title="Upcoming Games" navigation={navigation} />
        ),
        headerTransparent: true,
        
      })
    }
  },
  {
    cardStyle: {
      backgroundColor: "#F8F9FE"
    },
    transitionConfig
  }
);
// divideru se baga ca si cum ar fi un ecrna dar nu-i nimic duh
const AppStack = createDrawerNavigator(
  {
    Onboarding: {
      screen: Onboarding,
      navigationOptions: {
        drawerLabel: () => {}
      }
    },
    Home: {
      screen: HomeStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} title="Home" />
        )
      })
    },
    Profile: {
      screen: ProfileStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="Profile" title="Profile" />
        )
      })
    },
    Account: {
      screen: Register,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="Register" title="Account" />
        )
      })
    },
    Elements: {
      screen: ElementsStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="Elements" title="Elements" />
        )
      })
    },
    Articles: {
      screen: ArticlesStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="Articles" title="Articles" />
        )
      })
    }
  },
  Menu
);




const AppContainer = createAppContainer(AppStack);
export default AppContainer;
