import { AppRegistry } from "react-native";
import React from 'react';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducer from './store/reducers/HomescreenReducer';
import thunk from 'redux-thunk'

import Home from "./screens/Home";

const logger = store=>{
    return next =>{
        return action=>{
            console.log('[Middleware] dispatching ',action);
            const result = next(action);
            console.log('[Middleware] next state :-', store.getState());
            return result
        }
    }
}

const store = createStore(reducer,applyMiddleware(logger,thunk));

const AppContainer = ()=>{
    <Provider store={store}>
        <Home />
    </Provider>
}

AppRegistry.registerComponent("reduxlearningstarter", () => AppContainer);