var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
const mongooseDateFormat = require('mongoose-date-format');
require('mongoose-type-email');

var Schema = mongoose.Schema;
var gamesSchema = new Schema({
    name: String,
    owner: String,
    owneremail:mongoose.SchemaTypes.Email,
    gametype: String,
    freebies: {type: Boolean,  default: false},
    pplcount: {type:Number, default: 0},
    description: String,
    startdate: Date,
    freebiesdescription: String,
    peoplenames:[{type:String}]
},{ collection: 'games' });

gamesSchema.plugin(mongooseDateFormat)
var gamesinfo = mongoose.model('GamesInfo', gamesSchema);

module.exports = gamesinfo;