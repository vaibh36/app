
const express = require('express');
const bodyParser = require('body-parser')
const app = express();
var http = require('http');
var mongoose = require('mongoose');
var db = 'mongodb://localhost/vaibhav';
const mongooseDateFormat = require('mongoose-date-format');
var Gamesinfo = require('./models/Game.model');
var moment = require('moment');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
var GameInfo = require('./models/Game.model');

mongoose.connect(db)
    .then(() => {
        console.log('Connected to database');
    })
    .catch(() => {
        console.log("Connection failed");
    });

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Header", "Origin,X-Requested-With, Content-Type,Accept");

    res.setHeader("Access-Control-Allow-Methods", "GET,POST,PATCH,DELETE,OPTIONS")

    next();
})

app.get('/getPeopleCount', (req, res) => {
    console.log('Inside the API to find the numer of people associated with the game');

    res.status(201).json({
        message: 'Response with field is sent',
        count: 2201
    })

});

app.get('/getUpcomingGames', (req, res) => {

   const todaysDate = new Date();
   console.log('Todays date is:-', todaysDate)
    Gamesinfo.find({ startdate : {$gte: todaysDate}}, function (err, docs) {
        if (!err) {
         //   console.log('Dates are:-', docs);       
            let ObjArray=[];
            for(let i=0; i<docs.length;i++){
                let nMessage;
                nMessage = moment(docs[i].startdate).fromNow();
                console.log('Message is:-', nMessage)
                console.log('Iteration number is:-',i);
                let newObj;
                newObj = {
                    message:nMessage,
                    pplCount:docs[i].pplcount,
                    owner: docs[i].owner,
                    gametype:docs[i].gametype,
                    name: docs[i].name,
                    freebees: docs[i].freebies, 
                    description: docs[i].description,
                    startdate:docs[i].startdate,
                    owneremail: docs[i].owneremail
                };
                ObjArray.push(newObj)
            }

            console.log('Final array is:-', ObjArray);
            res.status(201).json({
                upcomingscreendata: ObjArray
            })
        } else { throw err; }
    });
})

app.post('/createNewGame', (req,res, next)=>{
    console.log('Trying to create new game:-', req.body);

    const gameInfo = new GameInfo();
    gameInfo.name = req.body.info.title;
    gameInfo.owner = req.body.info.NickName;
    gameInfo.gametype = req.body.info.duration;
    gameInfo.freebies = req.body.info.Freebies;
    gameInfo.startdate = req.body.info.chosenDate;
    gameInfo.freebiesdescription = req.body.info.FreeBiesDesription;

   gameInfo.save()
   .then((result)=>{
    console.log('Items saved:-', result)
    res.status(201).json({
        message:'Your game has been created',
        result: result
    })
   })
   .catch((err)=>{
        console.log('Error is:-', err);
        res.send({
            message: 'Error is:-',err
        })
   })

})

module.exports = app;