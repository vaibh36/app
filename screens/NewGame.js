import React, { useState } from 'react';
import { View, Text, StyleSheet, TextInput, Button, CheckBox, Picker, TouchableOpacity} from 'react-native';
import DateTime from '../components/DateTime';
import { useSelector, useDispatch } from 'react-redux';
import {createNewGame} from '../store/actions/NewGameScreensAction';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Icon from '../components/Icon';
import moment from 'moment';
import { withNavigation } from 'react-navigation';


const NewGame = (props) => {

    console.log('Inside new game screen');
    const { navigation } = props;
    const newgamemessage = useSelector(state => state.newgamescreenreducer.newgamemessage);
    const dispatch = useDispatch();
    const [title, setTitle] = useState('');
    const [titleIsValid, setTitleIsValid] = useState(false);

    const [NickName, setNickname] = useState('');
    const [nicknameIsValid, setnicknameIsValid] = useState(false);

    const [Freebies, setFreebiesDescription] = useState(false);

    const [FreeBiesDesription, setFreeBiesDescriptionText] = useState('');
    const [FreeBiesDescriptionIsValid, setFreeBiesDescriptionTextIsValid] = useState(true);

    const [duration, setDuration] = useState('1d');

    const [isVisible, setIsVisible] = useState(false)
    const [chosenDate, setChosenDate] = useState()

    const createMyNewGame=()=>{
        console.log('Trying to create new game');
        if(!nicknameIsValid && !titleIsValid || (Freebies && !FreeBiesDescriptionIsValid)){
            console.log('No new game to be created');
        }else{
            console.log('Create Game');
            console.log('The request parameters are:-',title,NickName,Freebies,FreeBiesDesription,duration,chosenDate);
            const fdata = {
                title:title,
                NickName:NickName,
                Freebies:Freebies,
                FreeBiesDesription:FreeBiesDesription,
                duration:duration,
                chosenDate:chosenDate
            }
            dispatch(createNewGame(fdata));
            console.log('Data for the new game from the reducer is:-',newgamemessage)
            navigation.navigate('GameCreated')
        }
    }

       handlePicker = (datetime) => {
        // this.setState({
        //     isVisible: false,
        //     chosenDate: moment(datetime).format('MMMM, Do YYYY HH:mm')
        // })
        setIsVisible(false);
        setChosenDate(moment(datetime).format('YYYY-MM-DD HH:mm:ss'))
    }

    hidePicker = () => {
        setIsVisible(false)
    }

    showPicker = () => {
        setIsVisible(true)
    }

    const titleChangeHandler = (text) => {


        if (text.trim().length === 0) {
            setTitleIsValid(false)
        } else {
            setTitleIsValid(true)
        }
        setTitle(text);
    }

    const nicknameChangeHandler = (text) => {

        if (text.trim().length === 0) {
            setnicknameIsValid(false)
        } else {
            setnicknameIsValid(true)
        }
        setNickname(text);
    }

    const FreeBiesDescription = (text) => {
        if (Freebies) {
            if (text.trim().length === 0) {
                setFreeBiesDescriptionTextIsValid(false)
            } else {
                setFreeBiesDescriptionTextIsValid(true)
            }
            setFreeBiesDescriptionText(text)
        } else {
            setFreeBiesDescriptionTextIsValid(false)
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.formControl}>
                <Text style={styles.label}>Game Title</Text>
                <TextInput
                    value={title}
                    style={styles.input}
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={titleChangeHandler}
                />
                {!titleIsValid && <Text style={{ color: 'red' }}>Please enter a valid Game Title</Text>}
            </View>
            <View style={styles.formControl}>
                <Text style={styles.label}>Nick Name</Text>
                <TextInput
                    value={NickName}
                    style={styles.input}
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={nicknameChangeHandler}
                />
                {!nicknameIsValid && <Text style={{ color: 'red' }}>Please enter a valid Nick Name</Text>}
            </View>
            <View style={styles.formControl}>
                <CheckBox value={Freebies} onChange={() => {
                    setFreebiesDescription(!Freebies)
                    {
                        if (Freebies) {
                            setFreeBiesDescriptionTextIsValid(true)
                        } else {
                            setFreeBiesDescriptionTextIsValid(false)
                        }
                    }
                }} />
                <Text style={styles.label}>Freebies Description</Text>
                <TextInput
                    style={styles.input}
                    autoCorrect
                    returnKeyType="next"
                    onChangeText={FreeBiesDescription}
                    value={FreeBiesDesription}
                />

                {!FreeBiesDescriptionIsValid && <Text style={{ color: 'red' }}>Please enter Freebies description</Text>}
            </View>
            <View style={styles.formControl}>
                <Text style={styles.label}>Description</Text>
                <TextInput
                    style={styles.input}
                    autoCorrect
                />
            </View>
            <View>
                <Picker
                    selectedValue={duration}
                    style={{ height: 50, width: 100, color: 'blue' }}
                    onValueChange={(itemValue, itemIndex) =>
                        setDuration(itemValue)
                    }>
                    <Picker.Item label="1d" value="1d" />
                    <Picker.Item label="3d" value="3d" />
                    <Picker.Item label="7d" value="7d" />
                </Picker>
            </View>
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity onPress={this.showPicker} style={{paddingRight:10}}>
                    <Icon
                        family="Entypo"
                        size={16}
                        name="calendar"
                    />
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={isVisible}
                    onConfirm={this.handlePicker}
                    onCancel={this.hidePicker}
                    minimumDate={new Date(Date.now() + (24 * 60 * 60 * 1000))}
                />
                <Text>
                    {chosenDate}
                </Text>
            </View>
            <Button onPress={createMyNewGame} title="Create Game" />
        </View>
    )


};


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    formControl: {
        width: '100%'
    },
    input: {
        paddingHorizontal: 2,
        paddingVertical: 5,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1
    },
    label: {
        marginVertical: 5
    },
});


export default withNavigation(NewGame);