import React,{Component} from 'react';
import firebase from 'firebase';
import {ActivityIndicator} from 'react-native'

class Loading extends Component{

    componentDidMount(){
        this.checkIfLoggedIn();
    }

    checkIfLoggedIn = ()=>{

        firebase.auth().onAuthStateChanged((user)=>{
            if(user){
                this.props.navigation.navigate('Home')
            }else{
                this.props.navigation.navigate('Login')
            }
        })

    }

    render(){
        <ActivityIndicator />
    }

};

export default Loading;