import React from 'react';
import { View, StyleSheet } from 'react-native';
import { } from 'react-native';
import { Block, Text, theme, Button } from 'galio-framework';
import { Ionicons } from '@expo/vector-icons';
import Icon from '../../walkingpledge/components/Icon';

const gamedescription = (props) => {

    console.log('Description is:-', props.navigation.getParam('description'))
    let gift = props.navigation.getParam('freebees');
    console.log('Gift in the screen is:-', gift)
    if(props.navigation.getParam('freebees')===true){
        gift = 'true'
    } else 
    gift = 'false';

    const JoinGame= ()=>{
        console.log('About to join the game');
        props.navigation.navigate('GameJoin')
    }


    return (
        <View style={{ paddingLeft: 30 }}>
            <Block middle style={styles.container} style={{ paddingTop: 30 }}>
                <Text style={styles.heading}>{props.navigation.getParam('title')} </Text>
            </Block>
            <View style={{ alignItems: 'flex-start' }} >
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons name="ios-person" size={35} style={styles.sizing}></Ionicons>
                    <Text style={styles.sizing}>{props.navigation.getParam('owner')} </Text>
                    <View style={{alignSelf: 'center', paddingLeft: 50, flexDirection: 'row', alignItems: 'center'}}>
                        <Icon name="star" family="Entypo"></Icon>
                        <Text style={styles.sizing}>3.5</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Ionicons name="md-calendar" size={35} style={styles.sizing}></Ionicons>
                    <Text style={styles.sizing}>{props.navigation.getParam('gameduration')} </Text>
                </View>
                <View style={{ flexDirection: 'row',alignItems: 'center', justifyContent: 'center' }}>
                    <Ionicons name="md-gift" size={35} style={styles.sizing}></Ionicons>
                    <Text style={styles.sizing}>{gift}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Ionicons name="md-people" size={35} style={styles.sizing}></Ionicons>
                    <Text style={styles.sizing}>{props.navigation.getParam('pplCount')} </Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Ionicons name="md-people" size={35} style={styles.sizing}></Ionicons>
                <Text style={styles.sizing}>{props.navigation.getParam('startdate')} </Text>
            </View>
            </View>

            <View style={{ padding: 10 }}>
                <Text>{props.navigation.getParam('description')}</Text>
            </View>

            <View>
                <Button  style={{paddingRight: 25}} onPress={()=>{
                        JoinGame()
                }}>Join</Button>
            </View>

        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    heading: {
        fontWeight: 'bold',
        fontSize: 20,
        paddingBottom: 10
    },
    innerBlockAlignmet: {
        paddingLeft: 20,
    },
    sizing: {
        color: 'blue',
        fontWeight: 'bold',
        padding: 10,
        fontSize: 20

    }
})

export default gamedescription;