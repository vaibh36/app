import React, { useEffect, useCallback } from 'react';
import { View, Text, FlatList, StyleSheet, Dimensions } from 'react-native';
import articles from '../constants/articles';
import Card from '../components/Card';
import gamedescription from '../constants/gamedescription';
import { Block, theme } from 'galio-framework';
const { width } = Dimensions.get('screen');
import { useSelector, useDispatch } from 'react-redux';
import { getUpcomingData } from '../store/actions/UpcominggamescreenActions';
import RenderArticles from '../components/RenderArticles';

const upcominggames = () => {

  console.log('[render] UpcomingGames screen:-');
  const loggedemail = 'anshuman78@gmail.com'
  const [refreshing, setRefreshing] = React.useState(false);
  const dispatch = useDispatch()
  const upcomingdata = useSelector(state => state.upcomingscreenreducer.UpcomingScreenData);
 // console.log('Data from the API for upcoming screen is:-',upcomingdata);
  let upComingData = [];

  const newFunc = useCallback(() => {
    dispatch(getUpcomingData());
    setRefreshing(false);
  })

  useEffect(() => {
    console.log('Inside use effect of Upcoming games')
    newFunc();
    setRefreshing(false);
  }, [])


//  upComingData = upcomingdata.upcomingscreendata;
  upComingData = upcomingdata;
  if (upComingData) {
    console.log(upComingData.length)
  }

   handleRefresh = () => {
    setRefreshing(true);
    newFunc();
  }

  renderArticles = () => {
    return (
      // <RenderArticles upComingData={upComingData} componentName="getUpcomingGamesData" />
       <FlatList refreshing={refreshing} onRefresh={this.handleRefresh} keyExtractor={item => item.id} data={upComingData} renderItem={({ item }) => (
        <Card loggedemail={loggedemail} owneremail={item.owneremail} startdate={item.startdate} daysleft={item.message} gift={item.freebees} pplCount={item.pplCount} duration={item.gameduration} item={item}  ></Card>
      )}
     /> 
    )
  }

  return (
    <Block flex center style={styles.home}>
      {this.renderArticles()}
    </Block>
  )

};

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
});

export default upcominggames;