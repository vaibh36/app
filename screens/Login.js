import React, { Component } from 'react';
import { Text, Button, StyleSheet, View, TouchableOpacity } from 'react-native';
import * as Google from 'expo-google-app-auth';
//import Expo from 'expo-google-sign-in'
//import * as GoogleSignIn from 'expo-google-sign-in';
import firebase from 'firebase';
import Icon from '../components/Icon';
import { MaterialCommunityIcons } from '@expo/vector-icons';

class Login extends Component {

    isUserEqual = (googleUser, firebaseUser) => {
        if (firebaseUser) {
            var providerData = firebaseUser.providerData;
            for (var i = 0; i < providerData.length; i++) {
                if (providerData[i].providerId === firebase.auth.GoogleAuthProvider.PROVIDER_ID
                    //    &&  providerData[i].uid === googleUser.getBasicProfile().getId()
                ) {
                    // We don't need to reauth the Firebase connection.
                    return true;
                }
            }
        }
        return false;
    }


    onSignIn = (googleUser) => {
        console.log('Google Auth Response', googleUser);
        // We need to register an Observer on Firebase Auth to make sure auth is initialized.
        var unsubscribe = firebase.auth().onAuthStateChanged(function (firebaseUser) {
            unsubscribe();
            // Check if we are already signed-in Firebase with the correct user.
            if (!this.isUserEqual(googleUser, firebaseUser)) {
                // Build Firebase credential with the Google ID token.
                var credential = firebase.auth.GoogleAuthProvider.credential(
                    //googleUser.getAuthResponse().id_token
                    googleUser.idToken,
                    googleUser.accessToken
                );
                // Sign in with credential from the Google user.
                firebase.auth().signInWithCredential(credential)
                    .then(() => {
                        console.log('USer signed in');
                //        this.props.navigation.navigate('Home');
                        this.props.navigation.navigate({
                            routeName: 'Home',params:{
                                email:'anshuman78@gmail.com'
                            }

                        })
                    })
                    .catch(function (error) {
                        // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // The email of the user's account used.
                        var email = error.email;
                        // The firebase.auth.AuthCredential type that was used.
                        var credential = error.credential;
                        // ...
                    });
            } else {
                console.log('User already signed-in Firebase.');
                this.props.navigation.navigate('Home')
            }
        }.bind(this));
    }

    signInWithGoogleAsync = async () => {
        console.log('Inside here')
        try {

            const result = await Google.logInAsync({
                behavior: 'web',
                androidClientId: '304811294137-eh58gh4a2s6v3nghoh6ldc5qo736dp76.apps.googleusercontent.com',
                //  iosClientId: YOUR_CLIENT_ID_HERE,
                scopes: ['profile', 'email'],
            });

            if (result.type === 'success') {
                this.onSignIn(result)
                return result.accessToken;
            } else {
                return { cancelled: true };
            }
        } catch (e) {
            console.log('Inside catch of signInWithGoogleAsync:-', e)
            return { error: true };
        }
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ paddingBottom: 10 }}>Login with Google</Text>
                <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={{ backgroundColor: '#f194ff'}}>
                        <MaterialCommunityIcons backgroundColor="#f194ff"    name="gmail" size={32}></MaterialCommunityIcons>
                      
                    </TouchableOpacity>
                    <Button color="#f194ff" style={styles.LoginButton} title="Google" onPress={() => this.signInWithGoogleAsync()} />
                </View>
            </View>
        )
    }

};
const styles = StyleSheet.create({
    LoginButton: {
        width: 45,
        height: 28
    },
    shadow: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 4 },
        shadowRadius: 4,
        shadowOpacity: 0.1,
        elevation: 2,
    },
});

export default Login;

