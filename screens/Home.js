import React from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList, View } from 'react-native';
import { Block, theme } from 'galio-framework';
import GameDescription from '../screens/GameDescription';
import { Card } from '../components';
import articles from '../constants/articles';
const { width } = Dimensions.get('screen');
import { connect } from 'react-redux';
import { getpplCount } from '../store/actions/HomescreenActions';
import { useSelector, useDispatch } from 'react-redux'
import * as gamedescription from '../constants/gamedescription';

const Home = (props) => {
//  console.log('Logged in users email is:-', props.navigation.getParam('email'))
  const loggedemail = 'anshuman78@gmail.com'
  const availabeData = useSelector(state => state.homereducer.pplCount)
  console.log('hook data is:-', availabeData, width);
  const dispatch = useDispatch()
  
  renderArticles = () => {
    return (
        <FlatList keyExtractor={item => item.id} data={articles} renderItem={({ item }) => (
            <Card loggedemail={loggedemail} owneremail={item.owneremail} daysleft={item.daysleft} duration={item.gameduration} description={gamedescription.description} item={item}  pplCount={availabeData} ></Card>
        )}
        />
    )
  }

  console.log('[render] Homescreen:-', gamedescription.description);
  dispatch(getpplCount())
  return (
    <Block flex center style={styles.home}>
      {this.renderArticles()}
    </Block>
  );
}

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  articles: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE,
  },
});


export default Home;
