import React, { useEffect, Component } from 'react';
import { View, CheckBox, Text, Button } from 'react-native';
//var gapi = require('gapi');
import googleFit, {GoogleFit} from 'react-native-google-fit'



class gamejoiningscreen extends Component {
    
/*

{"installed":{"client_id":"693188479349-9bt59ihdlblr7n686su0nf638o7jl7jv.apps.googleusercontent.com","project_id":"fitness-project-262607","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://oauth2.googleapis.com/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","redirect_uris":["urn:ietf:wg:oauth:2.0:oob","http://localhost"]}}

*/

    loadGoogleAPI = () => {
        console.log('Loading GoogleAPI')
        const script = document.createElement('script');
        script.src = 'https://apis.google.com/js/api.js';
        script.onload = () => {
            gapi.load('client', () => {
              gapi.client.setApiKey(API_KEY);
            });
          };
          document.body.appendChild(script);
    }

    authenticate = () => {
        return gapi.auth2.getAuthInstance()
            .signIn({ scope: "https://www.googleapis.com/auth/fitness.activity.read https://www.googleapis.com/auth/fitness.activity.write https://www.googleapis.com/auth/fitness.blood_glucose.read https://www.googleapis.com/auth/fitness.blood_glucose.write https://www.googleapis.com/auth/fitness.blood_pressure.read https://www.googleapis.com/auth/fitness.blood_pressure.write https://www.googleapis.com/auth/fitness.body.read https://www.googleapis.com/auth/fitness.body.write https://www.googleapis.com/auth/fitness.body_temperature.read https://www.googleapis.com/auth/fitness.body_temperature.write https://www.googleapis.com/auth/fitness.location.read https://www.googleapis.com/auth/fitness.location.write https://www.googleapis.com/auth/fitness.nutrition.read https://www.googleapis.com/auth/fitness.nutrition.write https://www.googleapis.com/auth/fitness.oxygen_saturation.read https://www.googleapis.com/auth/fitness.oxygen_saturation.write https://www.googleapis.com/auth/fitness.reproductive_health.read https://www.googleapis.com/auth/fitness.reproductive_health.write" })
            .then(function () { console.log("Sign-in successful"); },
                function (err) { console.error("Error signing in", err); });
    }

    loadClient = () => {
        gapi.client.setApiKey("YOUR_API_KEY");
        return gapi.client.load("https://content.googleapis.com/discovery/v1/apis/fitness/v1/rest")
            .then(function () { console.log("GAPI client loaded for API"); },
                function (err) { console.error("Error loading GAPI client for API", err); });
    }


    render() {

        return (
            <View>

                <Button onPress={() => this.initGapi()} title="Check connectivity" />
            </View>
        )
    }

};

export default gamejoiningscreen;