
import React from 'react';
import { Image } from 'react-native';
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import { Block, GalioProvider } from 'galio-framework';

import Screens from './navigation/Screens';
import { Images, articles, argonTheme } from './constants';
import {connect} from 'react-redux';
import { Provider } from 'react-redux';
import { createStore, combineReducers,applyMiddleware } from 'redux';
import reducer from '../walkingpledge/store/reducers/HomescreenReducer'
import ReduxThunk from 'redux-thunk';
import upcomingreducer from '../walkingpledge/store/reducers/UpcomingScreenReducer';
import newgamereducer from '../walkingpledge/store/reducers/NewGamescreenReducer';
import firebase from 'firebase';
import { firebaseConfig } from './config';

firebase.initializeApp(firebaseConfig)


const rootReducer = combineReducers({
  homereducer: reducer,
  upcomingscreenreducer:upcomingreducer,
  newgamescreenreducer:newgamereducer
})

const store = createStore(rootReducer,applyMiddleware(ReduxThunk));


// cache app images
const assetImages = [
  // Images.Onboarding,
  // Images.LogoOnboarding,
  // Images.Logo,
  // Images.Pro,
  // Images.ArgonLogo,
  // Images.iOSLogo,
  // Images.androidLogo,
 // Images.walkingPledge
]; 

// cache product images
articles.map(article => assetImages.push(article.image));

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

 class App extends React.Component {
  state = {
    isLoadingComplete: false,
  }
  

  render() {
    if(!this.state.isLoadingComplete) {
      return (
        <Provider store={ store }>
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
        </Provider>
      );
    } else {
      return (
        <Provider store={ store }>
        <GalioProvider theme={argonTheme}>
          <Block flex>
            <Screens />
          </Block>
        </GalioProvider>
        </Provider>
      );
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      ...cacheImages(assetImages),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };

}

export default App;