import axios from 'axios';

export const FETCH_PEOPLECOUNT = 'FETCH_PEOPLECOUNT';
export const DISPLAY_COUNT = 'DISPLAY_COUNT';

export const getpplCount = () => {
    console.log('Inside the action1');
    return dispatch=>{
        fetch('http://192.168.1.10:4200/getPeopleCount')
        .then(response => response.json())
        .then((data)=>{
            console.log('this is after fetch:-',data.count);
            dispatch ({
                type: DISPLAY_COUNT,
                payload: data.count
            })
        })
    }
};
