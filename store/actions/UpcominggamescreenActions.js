
export const DISPLAY_UPCOMING_DATA = 'DISPLAY_UPCOMING_DATA';

export const getUpcomingData = () => {
    console.log('Inside the action of getDataCount for upcoming screen');
    return dispatch=>{
        fetch('http://172.16.159.123:4200/getUpcomingGames')
        .then(response => response.json())
        .then((data)=>{
        //    console.log('this is after fetch:-',data);
            dispatch ({
                type: DISPLAY_UPCOMING_DATA,
                payload: data.upcomingscreendata
            })
        })
    }
};
