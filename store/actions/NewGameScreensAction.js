export const CREATE_NEWGAME = 'CREATE_NEWGAME';
export const createNewGame = (mydata) => {
    console.log('Inside the action to create new game');
    return dispatch => {
        fetch("http://192.168.1.10:4200/createNewGame", {
            method: "post",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
               info: mydata
            })
        })
            .then(response => response.json())
            .then((data)=>{
                    console.log('this is after fetch:-',data);
                    dispatch ({
                        type: CREATE_NEWGAME,
                        payload: data.message
                    })
                })
    }
};

