
const initialState = {
    pplCount:0
}
const reducer = (state=initialState,action)=>{
  //  console.log('Inside the reducer:-',action.payload)
    if(action.type==='DISPLAY_COUNT'){
        return{
            ...state,
            pplCount: action.payload
        }
    }
    return state;
}

export default reducer;