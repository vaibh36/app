import {CREATE_NEWGAME} from '../actions/NewGameScreensAction';

const initialState = {
    newgamemessage: ''
}

const reducer = (state=initialState,action)=>{
    console.log('Inside the reducer:-',action.payload)
    if(action.type===CREATE_NEWGAME){
        return{
            ...state,
            newgamemessage: action.payload
        }
    }
    return state;
}

export default reducer;