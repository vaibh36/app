import {DISPLAY_UPCOMING_DATA} from '../actions/UpcominggamescreenActions';

const initialState = {
    // pplCount:0,
    // name:'',
    // owner:'',
    // gametype:'',
    // message:'',
    // startdate:'',
    // freebees: false,
    UpcomingScreenData:[]

}

const reducer = (state=initialState,action)=>{
        switch(action.type){
            case DISPLAY_UPCOMING_DATA:
               const upcomingscreendata = action.payload;
               return{
                   ...state,
                   UpcomingScreenData: upcomingscreendata
               }
            default:
                return state;
        }
   
  
}

export default reducer;